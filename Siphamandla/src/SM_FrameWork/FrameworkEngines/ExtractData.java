package SM_FrameWork.FrameworkEngines;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

/**
 *
 * @author siphamandla
 */
public class ExtractData
{

    ArrayList ExcelData = new ArrayList();
    static String FileUrl = "";
    static String ExtractRow = "";
    static int TestCaseNum = 0;
    static String keywords = "";
    static FileInputStream fs;
    boolean checkKeywordPresence;
    int numberOfRws = 0;
    static Sheet sh;
    Workbook kywrd;
    static int totalNoOfRows;
    static int totalNoOfCols;
    int row = 0;

    public ExtractData(String TestPack) throws IOException, BiffException
    {

        this.fs = new FileInputStream(TestPack);
        this.kywrd = Workbook.getWorkbook(fs);
        this.sh = kywrd.getSheet("Sheet1");
        totalNoOfRows = sh.getRows();
        totalNoOfCols = sh.getColumns();

    }

    public static void updateRowNum(int i)
    {
        TestCaseNum = i;
    }

    public String getDataRequired(String SearchFor) throws IOException, BiffException //This method gets the required Data information required in a test.
    {
        String[] FoundMatch;
        String temp = "";
        String toSplit = "";
        try
        {

            int keepTrackOfRow = 0;

            if (totalNoOfRows == 0)
            {
             System.err.println("{Error Message} - The ExcelFile Has No data.");
            }
            else
            {
                while (TestCaseNum < totalNoOfRows)
                {
                    for (int col = 0; col < totalNoOfCols; col++)
                    {
                        //your comparision code goes here
                        if (sh.getCell(col, TestCaseNum).getContents().contains(SearchFor))
                        {
                            temp = sh.getCell(col, TestCaseNum).getContents();
                            FoundMatch = temp.split("=");
                            toSplit = FoundMatch[1];
                            break;
                        }
                    }
                    break;
                }
                fs.close();   
            }
        }
        catch (IOException e)
        {
            System.err.println("{URL Error Message :- Invalid URL }");
        }

        return toSplit;
    }

}
