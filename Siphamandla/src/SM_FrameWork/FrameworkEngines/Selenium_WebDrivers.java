package SM_FrameWork.FrameworkEngines;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import static SM_FrameWork.FrameworkEngines.Environment.GiveBrowser;
import static SM_FrameWork.FrameworkEngines.Reporting.WriteToFile;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author Siphamandla Description : This is the Selenium Webdriver class that
 * is used to communicate with WebBrowser Element during Test.
 */
public class Selenium_WebDrivers {

    public static WebDriver Driver;
    static String url = "";
    public String Test_Log = "";
    static String BrowserType;

    public static void SeliniumDriversProperty()// setting up Chrome and IE browser properties 
    {

        System.setProperty("webdriver.chrome.driver", "Drivers\\chromedriver.exe");
        System.setProperty("webdriver.ie.driver", "Drivers\\IEDriverServer.exe");

    }

    public static String Browsertype() {

        return BrowserType;
    }

    public static void maximizeBrowser()//maximizing browser
    {

        Driver.manage().window().maximize();
    }

    public static void Chrome(String Url)//running chrome browser
    {

        url = Url;

    }

    public static void FireFox(String Url) //running firefox Browser
    {

        url = Url;

    }

    public static void IE(String Url)//running Internet Explore Driver 
    {

        url = Url;

    }

    public static boolean isBrowserClosed()//this method ensure that the browser is closed 
    {

        if (Driver == null) {
            return true;
        }
        return false;
    }

    public static void setCapabilities() {// setting Driver capabilities 
        Capabilities cap = ((RemoteWebDriver) Driver).getCapabilities();
        BrowserType = cap.getBrowserName();

    }

    public void myUrl()//this method excutes web driver -  NB : Make sure this method is Called only on loggin classes 
    {
        if (isBrowserClosed()) { // ensures that the browser is closed
            System.out.println("-----------------------------LAUNCHING BROWSER----------------------");
            System.out.println("--------------------------------------------------------------------");

            if (GiveBrowser().equalsIgnoreCase("IE")) {

                SeliniumDriversProperty();
                Driver = new InternetExplorerDriver();
                maximizeBrowser();
                setCapabilities();
                Driver.get(url);
                Driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            }
            if (GiveBrowser().equalsIgnoreCase("Chrome")) {

                SeliniumDriversProperty();
                Driver = new ChromeDriver();
                maximizeBrowser();
                setCapabilities();
                Driver.get(url);
                Driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            }

            if (GiveBrowser().equalsIgnoreCase("FireFox")) {
                Driver = new FirefoxDriver();
                maximizeBrowser();
                setCapabilities();
                Driver.get(url);
                Driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            }
        } else {
            Driver.get(url);
            Driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        }

    }

    public static boolean isClosed()//checking if the  browser is closed
    {
        if (Driver == null) {
            return true;
        }
        return false;
    }

    public static void CloseBrowser()//Quiting the Selenium Driver
    {

        Driver.quit();
        Driver = null;
    }

//Selenium Actions 
    //...........................................................................................................
    public boolean ScrollToElement(String Xpath) throws IOException {
        WebElement element = null;
        try {
            Test_Log = "[SCROLL ACTION ] : " + Xpath;
            System.out.println(Test_Log);
            element = Driver.findElement(By.xpath(Xpath));
            ((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", element);

        } catch (Exception e) {
            Test_Log = "[SCROLL ACTION ERROR] : " + Xpath;
            System.err.println(Test_Log);
            WriteToFile(Test_Log);
            return false;
        }
        return true;
    }

    public boolean waitPageObjectToBeClickableByXpath_WithTimeOut(String Xapth, int Seconds) throws IOException {
        try {
            Test_Log = "[Waiting] : " + Xapth;
            System.out.println(Test_Log);

            WebDriverWait wait = new WebDriverWait(Driver, Seconds * 1000);
            WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Xapth)));
            WriteToFile(Test_Log);

        } catch (Exception e) {
            Test_Log = "[Waiting Error] : " + Xapth;
            System.err.println(Test_Log);
            WriteToFile(Test_Log);
            return false;
        }

        return true;
    }

    public boolean clickElement(String Xpath) throws IOException {
        boolean Found = true;
        try {
            Test_Log = "[Clicking] : " + Xpath;
            System.out.println(Test_Log);
            Driver.findElement(By.xpath(Xpath)).click();
            WriteToFile(Test_Log);
            return Found;
        } catch (Exception E) {
            Test_Log = "[Clicking Error] : " + Xpath;
            System.err.println(Test_Log);
            WriteToFile(Test_Log);
            Found = false;
            return Found;
        }

    }

    public boolean EnterByXpath(String Xpath, String ToEnter) throws IOException {
        boolean Found = true;
        try {
            Test_Log = "[Entering " + ToEnter + "] using " + Xpath;
            System.out.println(Test_Log);

            WebElement Enter = Driver.findElement(By.xpath(Xpath));
            Enter.sendKeys(ToEnter);
            WriteToFile(Test_Log);

        } catch (Exception E) {
            Test_Log = "[Entering Error of " + ToEnter + "] using " + Xpath;
            System.err.println(Test_Log);
            Found = false;
            WriteToFile(Test_Log);
            return Found;
        }

        return Found;
    }

    public boolean isChecked(String Xpath) {
        boolean isChecked = Driver.findElement(By.xpath(Xpath)).isSelected();
        if (isChecked) {
            return true;
        }
        return false;

    }

    public void pause(int seconds) throws InterruptedException {
        Thread.sleep(seconds * 1000);
    }

    public boolean getTextByXapth(String xpath) throws IOException {

        try {

            Test_Log = "[getText] using " + xpath;
            System.out.println(Test_Log);
            Driver.findElement(By.xpath(xpath)).getText();

            WriteToFile(Test_Log);
        } catch (Exception e) {
            Test_Log = "[getText Error] on " + xpath;
            System.err.println(Test_Log);
            WriteToFile(Test_Log);

            return false;
        }

        return true;
        //ScreenShots
    }

}
