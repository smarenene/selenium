/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SM_FrameWork.FrameworkEngines;
import static SM_FrameWork.FrameworkEngines.Environment.BrowserType.FireFox;
/**
 *
 * @author smarenene
 */
//**
public class Environment
{

    public static String myUrl = "";
    public static String browser = "";
    
    public static enum BrowserType //creating enum constants
    {

        FireFox, Chrome, IE;

    }
    
    public static String GiveBrowser()
    {
      return browser;  
    }

    public static enum Urls //creating enum url values 
    {

        Gmail("https://accounts.google.com/ServiceLogin?sacu=1&scc=1&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&hl=en&service=mail#identifier"),
        Google("https://www.google.co.za/?gfe_rd=cr&ei=IoOAWKGnPO2o8wfkrpSICA&gws_rd=ssl");
       
        
        public String url;

        Urls(String Url)//url parameterize constructoot 
        {
            this.url = Url;
           
        }

        public String ShowValue() //returns value of URL
        {

            return url;
        }

    }

    public static String RetreiveURL(String goToUrl)//getting the value of url

    {
        for (Urls Website : Urls.values())//iterating through url enum value

        {
            if (goToUrl.equalsIgnoreCase(Website.toString()))
            {
                String WebURL = Website.ShowValue();
                myUrl=goToUrl;
                return WebURL;
            }

        }

        return "";

    }

    public static void BrowserType(BrowserType Type, String url)//selecting browser and url base on option on the TestPack Run class
    {
        switch (Type)
        {

            case IE:
                
                browser = "IE";
                Selenium_WebDrivers.IE(RetreiveURL(url));
                break;

            case Chrome:
                browser = "Chrome";
                Selenium_WebDrivers.Chrome(RetreiveURL(url));
                break;

            case FireFox:
                browser = "FireFox";
                Selenium_WebDrivers.FireFox(RetreiveURL(url));
                break;
        }

    }

}
