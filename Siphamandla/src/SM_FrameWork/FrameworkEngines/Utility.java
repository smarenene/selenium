/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SM_FrameWork.FrameworkEngines;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author smarenene
 */
public class Utility {
    
    
      public static void CopyFileToAnotherDirectory(String source, String Destination) throws IOException {

        try {
            File sourcefile = new File(source);
            File distination = new File(Destination);
            FileUtils.copyDirectory(sourcefile, distination);
        } catch (Exception e) {

            System.err.println("Failed to copy the file to destination.");

        }
    }
    
}
