package SM_FrameWork.FrameworkEngines;

import static SM_FrameWork.FrameworkEngines.Reporting.multiMap;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.openqa.selenium.Keys;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author LUNATHI
 */
public class Parameters {

    private String Keyword = "";
    private String Description = "";
    private Multimap<String, String> Keywords = null;
    private FileInputStream fs;
    private static Sheet sh;
    private Workbook kywrd;
    int totalNoOfRows;
    int totalNoOfCols;
    public static int current_row = 0;

    public Parameters(String TestPack) throws FileNotFoundException, IOException, BiffException {
        Keywords = LinkedHashMultimap.create();
        fs = new FileInputStream(TestPack);
        kywrd = Workbook.getWorkbook(fs);
        sh = kywrd.getSheet("Sheet1");
        totalNoOfRows = sh.getRows();
        totalNoOfCols = sh.getColumns();
    }

    private  Multimap<String, String> Key() {

        for (int row = 0; row < totalNoOfRows; row++) {

            String key = sh.getCell(0, row).getContents();
            String descrip = sh.getCell( 1,row).getContents();
            Keywords.put(key, descrip);

        }

        return Keywords;
    }

    public  Multimap<String, String> MyKeyword()
    {
        return Key();
    }
    
}
