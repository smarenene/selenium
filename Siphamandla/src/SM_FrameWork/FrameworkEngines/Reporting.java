package SM_FrameWork.FrameworkEngines;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import static SM_FrameWork.FrameworkEngines.Selenium_WebDrivers.Browsertype;
import static SM_FrameWork.FrameworkEngines.Selenium_WebDrivers.Driver;
import static SM_FrameWork.FrameworkEngines.Utility.CopyFileToAnotherDirectory;
import static SM_FrameWork.TestCaseClass.testEngine.DrivenTest;
import static SM_FrameWork.TestCaseClass.testEngine.MethodRuning;
import static SM_FrameWork.TestCaseClass.testEngine.PassRowIndex;
import static SM_FrameWork.TestCaseClass.testEngine.TestPackName;
import static SM_FrameWork.TestCaseClass.testEngine.TotNumberOfRuns;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Siphamandla
 */
public class Reporting extends Instances {

    static ArrayList copyiterations;
    public static String userproperty = System.getProperty("user.name");
    public static String s = "";
    static ArrayList TestStep = new ArrayList();
    static ArrayList Status = new ArrayList();
    static ArrayList StatusMessage = new ArrayList();
    static Multimap<String, String> multiMap = LinkedHashMultimap.create();
    static ArrayList TableName = new ArrayList();
    static String FileUrl = "";
    static String ExtractRow = "";
    static int TestCaseNum = 0;
    static String keywords = "";
    static FileInputStream fs;
    boolean checkKeywordPresence;
    int numberOfRws = 0;
    static Sheet sh;
    static Workbook kywrd;
    static int totalNoOfRows;
    static int totalNoOfCols;
    int row = 0;
    ExtractData testData;
    String StoreData = "";
    int checkRuns = 0;
    String[] arrayKEY = new String[1];
    String StringData;
    static String methodRuning = "";
    String current = "";
    String previous = "";
    public static boolean CheckMethoDRunning;
    static String str;
    static String error = "";
    static String Directory = "";
    static int countrun = 0;
    static boolean validateFailMethod = true;
    static boolean FailPass;
    public String subReporting = "";
    boolean tmp;
    static int imageNumber = 0;
    static int i = 0;
    static String mykey = "";
    static String imagePaths = "";
    static String currentTime = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
    static String direct = TestPackName+"_" + currentTime;
    static String imagename = "";
    static boolean MethodFailed = true;
    static int runs = 0;
    static ArrayList RowIndex = new ArrayList();
    static int NumberOfFails = 0;
    static String whereDidItFail = "";

    public Reporting(String Path) throws FileNotFoundException, IOException, BiffException {

        this.fs = new FileInputStream(Path);
        this.kywrd = Workbook.getWorkbook(fs);
        this.sh = kywrd.getSheet("Sheet1");
        totalNoOfRows = sh.getRows();
        totalNoOfCols = sh.getColumns();
        this.StringData = Path;

    }

    public static void exitTest() {
        try {

            Reporting.WriteToHTMLFile();
            System.exit(0);
        } catch (Exception e) {
            System.out.println("[Error] - Failed to exit Test");
        }
    }

//end of CssFile Creator   dding steps to The array List and use it when generating the report
    public static void GenerateReport(String statusMssg, String myStatus, boolean checkClassFail) throws IOException {

        imagename = statusMssg;
        TakeScreenShot();//taking screen shot of reported status 
        if (!checkClassFail) {
            NumberOfFails++;

            validateFailMethod = checkClassFail;
            multiMap.put(DrivenTest(), statusMssg + "," + myStatus);

        } else {
            validateFailMethod = checkClassFail;
            multiMap.put(DrivenTest(), statusMssg + "," + myStatus);//adding status message to map

        }
    }

    public static void addTableName(String TestName) {
        TableName.add(TestName);
    }

//validating if there is a failed method 
    public static boolean ValidateFailedMethod() {
        return validateFailMethod;
    }

    public static boolean KeepTrackOfFailPassCase() {
        if (Driver == null) {
            FailPass = true;
        } else {
            FailPass = validateFailMethod;
        }

        return FailPass;

    }

    public void KeyWord2(int index) {

        mykey = sh.getCell(1, index).getContents();//getting contents of rows 

    }

    public static String RunningMethodImageDirectory() {

        return imagePaths;
    }

    public static boolean CheckFailPass(String Keys) {
        Set<String> keys = multiMap.keySet();
        boolean checkFailed_TestID = false;
        for (String key : keys) {
            Collection<String> collect = multiMap.get(key);
            for (Iterator<String> iterator = collect.iterator(); iterator.hasNext();) {
                String[] values = iterator.next().split(",");
                if (Keys.equalsIgnoreCase(key)) {

                    if (values[1].equalsIgnoreCase("Pass")) {
                        checkFailed_TestID = true;
                    } else if (values[1].equalsIgnoreCase("Failed")) {
                        whereDidItFail = values[0];
                        checkFailed_TestID =false;
                    }

                }
            }

        }
        return checkFailed_TestID;
    }

    public static void CreateReport() {

        PassRowIndex(RowIndex);//get the name of method from method name in testengine 
        int helper = 0;
        int rows = 0;
        int Counter = 0;
        int c = 0;
        int countTests = 1;
        String TableDataColor = "";
        String currentTime = new SimpleDateFormat("yyyy-MM-dd:HH.mm.ss").format(new Date());
        Set<String> keys = multiMap.keySet();
        int iter = 0;
        String bgColor = "";
        int pass = TotNumberOfRuns();
        String previousKey = "";
        String mystatus = "";

        s = "<Style>\n"
                + ".toggle-box {\n"
                + "  display: none;\n"
                + "}\n"
                + "\n"
                + ".toggle-box + label {\n"
                + "font-size:100%;"
                + "  cursor: pointer;\n"
                + "  display: block;\n"
                + "  font-weight: bold;\n"
                + "  line-height: 21px;\n"
                + "  margin-bottom: 5px;\n"
                + "}\n"
                + "\n"
                + ".toggle-box + label + div {\n"
                + "  display: none;\n"
                + "  margin-bottom: 10px;\n"
                + "}\n"
                + "\n"
                + ".toggle-box:checked + label + div {\n"
                + "  display: block;\n"
                + "  font-size: 300%;"
                + "}\n"
                + "\n"
                + ".toggle-box + label:before {\n"
                + "  background-color: #4F5150;\n"
                + "  -webkit-border-radius: 10px;\n"
                + "  -moz-border-radius: 10px;\n"
                + "  border-radius: 10px;\n"
                + "  color: #FFFFFF;\n"
                + "  content: \"+\";\n"
                + "  display: block;\n"
                + "  float: left;\n"
                + "  font-weight: bold;\n"
                + "  height: 20px;\n"
                + "  line-height: 20px;\n"
                + "  margin-right: 5px;\n"
                + "  text-align: center;\n"
                + "  width: 20px;\n"
                + "}\n"
                + "\n"
                + ".toggle-box:checked + label:before {\n"
                + "  content: \"-\";\n"
                + "}\n"
                + "img{width:80px;height:50px;}"
                + ""
                + "img:active {\n"
                + "   position: fixed; \n"
                + "  top: 0; \n"
                + "  left: 0; \n"
                + "	\n"
                + "  /* Preserve aspet ratio */\n"
                + "  min-width: 100%;\n"
                + "  min-height: 100%;  "
                + "}\n"
                + "* {\n"
                + "    -webkit-transition: all 0.5s ease-in-out;\n"
                + "    -moz-transition: all 0.5s ease-in-out;\n"
                + "    -ms-transition: all 0.5s ease-in-out;\n"
                + "    -o-transition: all 0.5s ease-in-out;\n"
                + "    transition: all 0.5s ease-in-out;\n"
                + "}"
                + "</style>\n"
                + " <link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">"
                + " <div id=\"testInformation\">"
                + "<br><b>   OS Version : </b>" + System.getProperty("os.name")
                + "<br><b>     Test Execution Date :</b> " + currentTime
                + "<br>    <b>Platform :</b> " + Browsertype()
                + "<br> <b>Enviroment Testing : </b>" + Environment.myUrl
                + "<br><br><b>Total Number Tests   : </b>" + TotNumberOfRuns() + "<br>"
                + "<b>Number of Failed Tests  : </b>" + NumberOfFails + "<br>"
                + "<b>Number of Passed Tests  : </b>" + (pass - NumberOfFails) + "</div><br>"
                + "</div>"
                + "<input type=\"text\" id=\"myInput\" onkeyup=\"Filter()\" placeholder=\"Filter by Typing Fail or Pass Status\">"
                + "<table id=\"myTable\">\n"
                + " <tr class=\"header\">\n"
                + "    <th style=\"width:60%;\">Test ID </th>\n"
                + "    <th style=\"width:40%;\">Test Status</th>\n"
                + "  </tr>";
        int z = 1;

        for (String key : keys) {

           // previousKey = key;

            Collection<String> collect = multiMap.get(key);
            imagePaths = key;//Create directory with the name of folder ;
            if (CheckFailPass(key)) {
                mystatus = " <font color= 'Green'>Pass</font>";
            } else {
                mystatus = " <font color= 'Red'> Fail </font> ";
            }
            String[] Run = key.split("_");
            s = s.concat(""
                    + "<tr><TD>"
                    + "<input class=\"toggle-box\" id='identifier-" + i + "' type=\"checkbox\">\n"
                    + "<label for='identifier-" + i + "'>" + Run[1] + "</label>\n <div>\n"
                    + "\n"
                    + " <table id=\"myTable2\" style=\"width:90%\">\n"
                    + "  <tr>\n"
                    + "    <td bgcolor=''>Test Results</td>\n"
                    + "    <td bgcolor=''>Screenshots</td>\n"
                    + "    <td>Status Message</td>\n"
                    + "  </tr>\n");

            String temporar = "";
            for (Iterator<String> iterator = collect.iterator(); iterator.hasNext();) {
                String[] values = iterator.next().split(",");
                try {
                    if (values[1].equalsIgnoreCase("Pass")) {
                        bgColor = "Lime";
                        s = s.concat("<tr bgcolor= '" + bgColor + "'>"
                                + "<td>" + values[0] + " </td>"
                                + "<td> <img class = 'enlarge' src='" + imagePaths + "//" + iter + "-" + values[0] + ".jpg'/></td>"
                                + "<td> Pass</td></tr>\n");
                    }

                    if (values[1].equalsIgnoreCase("Failed")) {
                        bgColor = "Pink";

                        s = s.concat("<tr bgcolor= '" + bgColor + "'>"
                                + "<td>" + values[0] + " </td>"
                                + "<td> <img class='enlarge' src='" + imagePaths + "//" + iter + "-" + values[0] + ".jpg'/></td>"
                                + "<td>Fail</td></tr>\n");
                    }
                    iter++;

                } catch (Exception e) {
                    break;
                }

            }

            s = s.concat(
                    "</table>"
            );

            s = s.concat(
                    "\n"
                    + "</TD>"
                    + "<td>" + mystatus + "</td>"
                    + "</TR> \n"
            );
            i++;
            helper = 0;
            rows++;
            c = c + 2;

        }

        s = s.concat("</table>\n"
                + "\n"
                + "\n"
                + "</div><br></form>   "
                + "<script>\n"
                + "function Filter() {\n"
                + "  var input, filter, table, tr, td, i;\n"
                + "  input = document.getElementById(\"myInput\");\n"
                + "  filter = input.value.toUpperCase();\n"
                + "  table = document.getElementById(\"myTable\");\n"
                + "  tr = table.getElementsByTagName(\"tr\");\n"
                + "  for (i = 0; i < tr.length; i++) {\n"
                + "    td = tr[i].getElementsByTagName(\"tr\")[2];\n"
                + "    if (td) {\n"
                + "      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {\n"
                + "        tr[i].style.display = \"\";\n"
                + "      } else {\n"
                + "        tr[i].style.display = \"none\";\n"
                + "      }\n"
                + "    }       \n"
                + "  }\n"
                + "}\n"
                + "</script>"
                + "</body>");

    }

    public void myDirectory() throws IOException {

        createDirectory();
        TestLogFile();
    }

    public static String getReportPath()//returning report Location
    {

        return Directory;

    }

    //creating Directory for 
    public static void createDirectory() {//creating directory for report Information

        String currentTime = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        Directory = "Artifacts_" + currentTime;
        Path dir = Paths.get("htmlReport\\" + Directory);
        if (!Files.exists(dir)) {
            try {
                Files.createDirectories(dir);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void ImagePaths(String Path) {//creating directory for report Information
        Directory = direct;
        Path images = Paths.get("htmlReport\\" + direct + "\\" + Path);
        try {
            Files.createDirectories(images);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

  

    public static void WriteToHTMLFile() throws IOException //writing to Html fie 
    {
        CopyFileToAnotherDirectory("cssstyle\\", "htmlReport\\" + Directory);
        CreateReport();//creating html table with report Information
        FileWriter fWriter = null;
        FileWriter Fcss = null;
        FileWriter xcelFile = null;
        BufferedWriter writer = null;
        File f = new File("htmlReport\\" + Directory + "\\fileName.html");
        try {
            //Creating Html File
            fWriter = new FileWriter("htmlReport\\" + Directory + "\\fileName.html");
            writer = new BufferedWriter(fWriter);
            writer.write(s);
            writer.newLine();
            writer.close();

        } catch (Exception e) {
            //catch any exceptions here
        }

    }

//taking Screenshots
    public static void TakeScreenShot() throws IOException {
        try {

            File scrFile = ((TakesScreenshot) Driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File("htmlReport\\" + Directory + "\\" + MethodRuning() + "\\" + (imageNumber + "-" + imagename) + ".jpg"));
            imageNumber++;

        } catch (IOException e) {

            System.out.println("[Error] -  Failed to Take ScreenShot");
        }

    }

    public void TestLogFile() throws IOException {
        File created = new File("htmlReport\\" + Directory + "\\TestLog.txt");
        try {
            created.createNewFile();

        } catch (Exception e) {

        }
    }

    public static void WriteToFile(String Content) throws IOException {

        try {

            File file = new File("htmlReport\\" + Directory + "\\TestLog.txt");
            BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));
            bw.write(Content);
            bw.newLine();
            bw.close();
        } catch (Exception e) {

        }
    }

}
