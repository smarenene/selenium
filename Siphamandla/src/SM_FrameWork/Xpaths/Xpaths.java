/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SM_FrameWork.Xpaths;

/**
 *
 * @author smarenene
 */
public class Xpaths {

    //loggin----------------------
    public static String LoginInput() {
        return "//input[@type='email']";
    }

    public static String NextButton() {
        return "//span[text()='Next']";
    }

    public static String Password() {
        return "//input[@type='password']";
    }

    public static String SignIn() {
        return "//span[text()='Next']";
    }

    public static String Image() {
        return "google.png";
    }

    //sent message
    public static String accessbyText(String text) {
        return "//div[text()='" + text + "']";
    }

}
