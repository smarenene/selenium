/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SM_FrameWork.TestCaseClass.Examples;

import SM_FrameWork.Xpaths.Xpaths;
import SM_FrameWork.FrameworkEngines.ExtractData;
import SM_FrameWork.FrameworkEngines.ExtractData;
import static SM_FrameWork.TestCaseClass.testEngine.AlertFailure;
import SM_FrameWork.FrameworkEngines.Instances;
import SM_FrameWork.FrameworkEngines.Reporting;

import java.io.IOException;
import jxl.read.biff.BiffException;

/**
 *
 * @author Siphamandla
 */
public class Compose extends Instances {

    String error = "";
    ExtractData Data;

    public Compose(String str) throws IOException, BiffException {
        this.Data = new ExtractData(str);

    }

    public boolean RunTests() throws IOException, BiffException, InterruptedException {

        if (!Go2SentMessages()) {
            Reporting.GenerateReport("Failed to view message link -" + error, "Failed", false);
            AlertFailure(true);
            return false;
        }

        return true;
    }

    public boolean Go2SentMessages() throws IOException, BiffException, InterruptedException {

        
        if (!SeleniumDriver.waitPageObjectToBeClickableByXpath_WithTimeOut(Xpaths.accessbyText(Data.getDataRequired("ComposeButton")), 30)) {
            error = "Fail to wait for click messages";
            return false;
        }

         Reporting.GenerateReport("Click sent message  is successfull", "Pass", true);
        if (SeleniumDriver.clickElement(Xpaths.accessbyText(Data.getDataRequired("ComposeButton")))) {
            error = "Fail to click sent message link";
            return false;
        }
        

        if (SeleniumDriver.waitPageObjectToBeClickableByXpath_WithTimeOut(Xpaths.accessbyText(Data.getDataRequired("SendButton")), 30)) {
            Reporting.GenerateReport("Click sent message  is successfull", "Pass", true);

        }else
        {
            return false;
        }

        // countrun++;
        return true;
    }

}
