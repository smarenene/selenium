/*

 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SM_FrameWork.TestCaseClass.Examples;

import SM_FrameWork.FrameworkEngines.ExtractData;
import static SM_FrameWork.TestCaseClass.testEngine.AlertFailure;
import SM_FrameWork.FrameworkEngines.Instances;
import SM_FrameWork.FrameworkEngines.Selenium_WebDrivers;
import SM_FrameWork.Xpaths.Xpaths;
import SM_FrameWork.FrameworkEngines.Reporting;

import java.io.IOException;
import jxl.read.biff.BiffException;

/**
 *
 * @author Siphamandla
 */
public class loggin extends Instances {

    String error = "";
    ExtractData Data;
    static boolean alert;

    public loggin(String str) throws IOException, BiffException {

        this.Data = new ExtractData(str);

    }

    public boolean RunTests() throws IOException, BiffException, InterruptedException {

        if (!login()) {
            Reporting.GenerateReport("Failed to Login to System -" + error, "Failed", false);
            AlertFailure(true);
            return false;
        }

        return true;
    }

    public boolean login() throws IOException, BiffException, InterruptedException {
        SeleniumDriver.myUrl();

        if (!SeleniumDriver.waitPageObjectToBeClickableByXpath_WithTimeOut(Xpaths.LoginInput(), 30)) {
            return false;
        }
        if (!SeleniumDriver.EnterByXpath(Xpaths.LoginInput(), Data.getDataRequired("Name"))) {
            return false;
        }

        if (!SeleniumDriver.waitPageObjectToBeClickableByXpath_WithTimeOut(Xpaths.NextButton(), 30)) {
            return false;
        }

        if (!SeleniumDriver.clickElement(Xpaths.NextButton())) {
            return false;
        }
        Reporting.GenerateReport("successfully Entered email", "Pass", true);
        if (!SeleniumDriver.waitPageObjectToBeClickableByXpath_WithTimeOut(Xpaths.Password(), 30)) {
            return false;
        }
        if (!SeleniumDriver.EnterByXpath(Xpaths.Password(), Data.getDataRequired("Password"))) {
            return false;
        }

        if (!SeleniumDriver.waitPageObjectToBeClickableByXpath_WithTimeOut(Xpaths.NextButton(), 30)) {
            error = "Failed to click signin button";
            return false;
        }

        if (!SeleniumDriver.clickElement(Xpaths.NextButton())) {
            error = "Failed to click signin button";
            return false;
        }

        Reporting.GenerateReport("Login is successfull", "Pass", true);

        return true;
    }

}
