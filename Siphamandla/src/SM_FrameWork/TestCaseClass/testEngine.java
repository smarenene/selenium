/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SM_FrameWork.TestCaseClass;

import SM_FrameWork.FrameworkEngines.ExtractData;
import SM_FrameWork.TestCaseClass.Examples.Compose;
import SM_FrameWork.TestCaseClass.Examples.loggin;
import static SM_FrameWork.FrameworkEngines.ExtractData.updateRowNum;
import static SM_FrameWork.FrameworkEngines.Selenium_WebDrivers.CloseBrowser;
import SM_FrameWork.FrameworkEngines.Reporting;
import static SM_FrameWork.FrameworkEngines.Reporting.ImagePaths;
import static SM_FrameWork.FrameworkEngines.Reporting.addTableName;
import java.beans.PropertyChangeEvent;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import static sun.font.GlyphLayout.done;

/**
 *
 * @author Siphamandla Description : This is the Engine Class that Execute
 * TestClasses based on Keywords from The DataPack(excel file).
 */
public class testEngine
{

    static int Iterations = 1;
    static String FileUrl = "";
    static String ExtractRow = "";
    static int TestCaseNum = 0;
    static String keywords = "";
    static FileInputStream fs;
    boolean checkKeywordPresence;
    int numberOfRws = 0;
    static Sheet sh;
    static Workbook kywrd;
    static int totalNoOfRows;
    static int totalNoOfCols;
    int row = 0;
    ExtractData testData;
    String StoreData = "";
    static int checkRuns = 0;
    String[] arrayKEY = new String[1];
    String StringData;
    static String methodRuning = "";
    String current = "";
    String previous = "";
    public static boolean CheckMethoDRunning;
    static String str;
    static String DrivenKey = "";
    static ArrayList indexRows = new ArrayList();
    static boolean checkFails = false;
    static boolean isFailure;
    static ArrayList Numiteration;
    static int NumberOfRuns = 0;
    static int NumberofTests = 0;
    String excelpath = "";
    public static String TestPackName = "";

    public testEngine(String Path) throws IOException, BiffException
    {
        //  super(Path);

        TestPackName=Path.substring(10);
        this.excelpath = Path;
        testPack();

        //this.KEYWORD = ProccessXcellFile.Keyword();
    }

    public void testPack() throws IOException, BiffException
    {

        try
        {
            testData = new ExtractData(excelpath);
            Reporting CreateDirectory = new Reporting(excelpath);
            CreateDirectory.myDirectory();
            fs = new FileInputStream(excelpath);
            kywrd = Workbook.getWorkbook(fs);
            sh = kywrd.getSheet("Sheet1");
            totalNoOfRows = sh.getRows();
            totalNoOfCols = sh.getColumns();
            this.StringData = excelpath;
            boolean isBrowserOpen = false;
        }
        catch (Exception e)
        {
                System.err.println("<Error> - Could not find the Excel File");
        }
    }

    public static boolean validateIFRunningSubMethods()
    {
        return CheckMethoDRunning;
    }

    public static int Runs()
    {
       return checkRuns;
    }

    public static int TotNumberOfRuns()
    {
        return NumberofTests;
    }

    public static String MethodRuning()
    {
        String metho = methodRuning;
        return metho;
    }

    public static String DrivenTest()
    {
        return DrivenKey;
    }

    public static int NumbersofRows()
    {
        return totalNoOfRows;
    }

    public static void PassRowIndex(ArrayList Index)
    {
        Index.addAll(indexRows);
    }

    public static boolean AlertFailure(boolean PopAlert)// This is a alert method that is used in Test Class to Alert The Test Engine If there is a failed method
    {
        isFailure = PopAlert;
        return isFailure;
    }

    public void KeyDrivenRun() throws BiffException, IOException, InterruptedException //this method Gets Keyword from excel file that will be used in TestEngine
    {
        String temp;
        String Star = "";
        boolean hepler = false;
        int progressbar = 0;
        if (totalNoOfRows == 0)
        {
            System.err.println("{Error Message} - The ExcelFile Has No data.");
        }
        else
        {
            while (row < totalNoOfRows)
            {

               // System.out.println("Progress " + progressbar + "%");
                if (row > totalNoOfRows)//ensure rows are outRange error 
                {
                    break;
                }

                checkRuns++;

                if (row == 0)
                {
                    previous = sh.getCell(0, row).getContents();
                }
                else
                {
                    previous = sh.getCell(0, row - 1).getContents();
                }
                Star = sh.getCell(0, row).getContents();

                if (Star.equals("end"))// closes the broswer if method Test Ends 
                {

                    CloseBrowser();

                }
                if (!isFailure)//checks if no previous method failded

                {
                    for (int col = 0; col < 2; col++)
                    {

                        temp = sh.getCell(col, row).getContents();//getting contents of rows 

                        if (col == 0)
                        {
                            methodRuning = sh.getCell(col, row).getContents();
                            ImagePaths(methodRuning);
                            DrivenKey = sh.getCell(0, row).getContents();
                            if (current.equalsIgnoreCase(previous) && row > 0)
                            {
                                CheckMethoDRunning = true;

                            }
                            else
                            {
                                CheckMethoDRunning = false;

                            }

                        }

                        if (col == 1)
                        {
                            if (temp.contains("Loggin"))//ensure if TestData is loggin and also ensure if there is no method fail
                            {
                                if (!hepler)
                                {
                                    //DrivenKey = sh.getCell(0, row).getContents();

                                    if (!temp.equalsIgnoreCase("end") && !temp.equalsIgnoreCase(""))
                                    {
                                        if (!temp.equalsIgnoreCase("end"))
                                        {
                                            indexRows.add(row);//create folder with a name of the method currently running
                                        }
                                    }
                                    if (!temp.equalsIgnoreCase(""))
                                    {
                                        NumberofTests++;
                                    }
                                    addTableName(temp);
                                    KeyDrivenTest(temp);//calling Keydrivenm method to run test base on keyword Argumented on it.
                                    break;
                                }
                            }
                            else if (!temp.contains("Loggin"))//do the below given if its not loggin and break if helper = True meaning Failed , or Run if there is no prevoius Method failed
                            {
                                if (hepler)
                                {
                                    break;

                                }
                                if (!hepler)
                                {
                                    if (!temp.equalsIgnoreCase(""))
                                    {
                                        NumberofTests++;
                                    }
                                    // DrivenKey =sh.getCell(0, row).getContents();
                                    temp = sh.getCell(col, row).getContents();
                                    addTableName(temp);
                                    KeyDrivenTest(temp);//call keydriven class to run class base on argumented keyword
                                    break;

                                }

                            }

                        }
                    }
                }
                else
                {
                    continue;
                }

                row = row + 2;
                checkRuns++;
                updateRowNum(row);//updating rows
                hepler = isFailure;
                isFailure = false;
                

            }

            System.out.println("--------------------------------------------------------------------");
            System.out.println("----------------------------------END OF TEST-----------------------");
            Reporting.WriteToHTMLFile();//writting report to html file
        }

        // return keywords;
    }

    public void ensureMethodRunOnceReset()//ensure that each method run once 
    {
        checkRuns = 0;

    }

    public void KeyDrivenTest(String KeyWord) throws BiffException, IOException, InterruptedException // This method is a keyword driven engine class that runs test classes base on Keywords from the ExcelFile.
    {
        switch (KeyWord)
        {
            case "Loggin":

                loggin myLogin = new loggin(StringData);
                myLogin.RunTests();
                break;
                
            case "Open Compose Message":
                Compose ComposeMessage = new Compose(StringData);
                ComposeMessage.RunTests();
                break;

        }

    }
}
