package TestClasses;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import SM_FrameWork.FrameworkEngines.Environment;
import SM_FrameWork.TestCaseClass.testEngine;
import org.junit.runner.RunWith;
import org.testng.annotations.Test;

/**
 *
 * @author smarenene
 */

public class Junit {

    testEngine test;
    
    @Test
    public void setUpClassA() throws Exception {
        Environment.BrowserType(Environment.BrowserType.FireFox, Environment.Urls.Gmail.name());//Sets User preference e.g Browsertype and Domain 
        test = new testEngine("DataPacks\\example.xls");//set excel file location
        test.KeyDrivenRun();//runs KeyDriven Method from the Test Engine
    }
      @Test
    public void setUpClassB() throws Exception {
        Environment.BrowserType(Environment.BrowserType.FireFox, Environment.Urls.Gmail.name());//Sets User preference e.g Browsertype and Domain 
        test = new testEngine("DataPacks\\example.xls");//set excel file location
        test.KeyDrivenRun();//runs KeyDriven Method from the Test Engine
    }
}
